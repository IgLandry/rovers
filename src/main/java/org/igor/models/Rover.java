package org.igor.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.igor.models.types.CardinalCompass;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Rover {

  private int abscissa;
  private int ordinate;
  private CardinalCompass direction;
  private String instructions;

  @Override
  public String toString() {

    return abscissa + " " + ordinate + " " + direction.name();
  }
}
