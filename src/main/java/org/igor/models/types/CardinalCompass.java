package org.igor.models.types;

public enum CardinalCompass {
  N,
  S,
  E,
  W
}
