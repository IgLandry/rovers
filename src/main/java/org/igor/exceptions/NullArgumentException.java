package org.igor.exceptions;

public class NullArgumentException extends IllegalArgumentException {

  public NullArgumentException(String errorMessage) {

    super(errorMessage);
  }
}
