package org.igor;

import java.util.List;
import java.util.logging.Logger;
import org.igor.models.Rover;
import org.igor.services.ManageRovers;
import org.igor.services.implementations.FileServicesImpl;
import org.igor.services.implementations.ManageRoverService;
import org.igor.services.implementations.RoverServicesImpl;

public class Main {

  private static final Logger myLog = Logger.getLogger("MyLog");

  public static void main(String[] args) {

    ManageRovers manageRoverService = new ManageRoverService(new FileServicesImpl(),
        new RoverServicesImpl());
    List<Rover> rovers = manageRoverService.manageRovers(args[0]);
    rovers.forEach(rover -> myLog.info(rover.toString()));
  }
}