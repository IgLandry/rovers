package org.igor.services;

import java.util.List;
import org.igor.models.Rover;

public interface ManageRovers {

  List<Rover> manageRovers(String fileName);
}
