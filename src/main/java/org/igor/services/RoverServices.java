package org.igor.services;

import org.igor.models.Rover;

public interface RoverServices {

  void stepForward(Rover rover, Rover plateau);

  void turnToTheRight(Rover rover);

  void turnToTheLeft(Rover rover);
}
