package org.igor.services.implementations;

import org.igor.exceptions.NullArgumentException;
import org.igor.models.Rover;
import org.igor.models.types.CardinalCompass;
import org.igor.services.RoverServices;

public class RoverServicesImpl implements RoverServices {

  private static final String ERROR_MESSAGE = "rover provided is null.";

  /**
   * @param rover to take a step forward.
   */
  @Override
  public void stepForward(Rover rover, Rover plateau) {

    if (rover != null && plateau != null) {
      final var height = plateau.getOrdinate();
      final var width = plateau.getAbscissa();
      final var vertical = rover.getOrdinate();
      final var horizontal = rover.getAbscissa();
      if (rover
          .getDirection()
          .compareTo(CardinalCompass.S) == 0 && vertical > 0) {
        rover.setOrdinate(rover.getOrdinate() - 1);
      }
      if (rover
          .getDirection()
          .compareTo(CardinalCompass.N) == 0 && vertical < height) {
        rover.setOrdinate(rover.getOrdinate() + 1);
      }
      if (rover
          .getDirection()
          .compareTo(CardinalCompass.E) == 0 && horizontal < width) {
        rover.setAbscissa(rover.getAbscissa() + 1);
      }
      if (rover
          .getDirection()
          .compareTo(CardinalCompass.W) == 0 && horizontal > 0) {
        rover.setAbscissa(rover.getAbscissa() - 1);
      }
    } else {
      throw new NullArgumentException(ERROR_MESSAGE);
    }
  }

  /**
   * @param rover to make a quarter turn to the right.
   */
  @Override
  public void turnToTheRight(Rover rover) {

    if (rover != null) {
      if (rover
          .getDirection()
          .compareTo(CardinalCompass.S) == 0) {
        rover.setDirection(CardinalCompass.W);
      } else if (rover
          .getDirection()
          .compareTo(CardinalCompass.N) == 0) {
        rover.setDirection(CardinalCompass.E);
      } else if (rover
          .getDirection()
          .compareTo(CardinalCompass.E) == 0) {
        rover.setDirection(CardinalCompass.S);
      } else if (rover
          .getDirection()
          .compareTo(CardinalCompass.W) == 0) {
        rover.setDirection(CardinalCompass.N);
      }
    } else {
      throw new NullArgumentException(ERROR_MESSAGE);
    }
  }

  /**
   * @param rover to make a quarter turn to the left.
   */
  @Override
  public void turnToTheLeft(Rover rover) {

    if (rover != null) {
      if (rover
          .getDirection()
          .compareTo(CardinalCompass.S) == 0) {
        rover.setDirection(CardinalCompass.E);
      } else if (rover
          .getDirection()
          .compareTo(CardinalCompass.N) == 0) {
        rover.setDirection(CardinalCompass.W);
      } else if (rover
          .getDirection()
          .compareTo(CardinalCompass.E) == 0) {
        rover.setDirection(CardinalCompass.N);
      } else if (rover
          .getDirection()
          .compareTo(CardinalCompass.W) == 0) {
        rover.setDirection(CardinalCompass.S);
      }
    } else {
      throw new NullArgumentException(ERROR_MESSAGE);
    }
  }
}
