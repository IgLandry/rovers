package org.igor.services.implementations;

import java.util.List;
import lombok.AllArgsConstructor;
import org.igor.models.Rover;
import org.igor.services.FileServices;
import org.igor.services.ManageRovers;
import org.igor.services.RoverServices;

@AllArgsConstructor
public class ManageRoverService implements ManageRovers {

  private FileServices fileServices;
  private RoverServices roverServices;

  /**
   * @param fileName input file;
   */
  @Override
  public List<Rover> manageRovers(String fileName) {

    final var rovers = fileServices.readInputFile(fileName);

    for (int i = 1; i < rovers.size(); i++) {

      var currentRover = rovers.get(i);
      var instructions = currentRover
          .getInstructions()
          .split("");

      for (String instruction : instructions) {

        if ("L".equals(instruction)) {
          roverServices.turnToTheLeft(currentRover);
        } else if ("R".equals(instruction)) {
          roverServices.turnToTheRight(currentRover);
        } else if ("M".equals(instruction)) {
          roverServices.stepForward(currentRover, rovers.get(0));
        }
      }
    }
    return rovers.subList(1, rovers.size());
  }
}
