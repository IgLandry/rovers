package org.igor.services.implementations;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.igor.models.Rover;
import org.igor.models.types.CardinalCompass;
import org.igor.services.FileServices;


public class FileServicesImpl implements FileServices {

  private final Logger myLog = Logger.getLogger("MyLog");


  /**
   * @param inputFile corresponding to the input file with initial values
   * @return List of rovers that have been extracted from the file.
   */
  @Override
  public List<Rover> readInputFile(String inputFile) {

    List<Rover> rovers = new ArrayList<>();
    try {

      var allLines = Files.readAllLines(Paths.get(inputFile), StandardCharsets.UTF_8);
      var plateau = allLines
          .get(0)
          .split(" ");

      rovers.add(Rover
          .builder()
          .abscissa(Integer.parseInt(plateau[0]))
          .ordinate(Integer.parseInt(plateau[1]))
          .build());

      for (int i = 1; i < allLines.size() - 1; i = i + 2) {
        var position = allLines
            .get(i)
            .split(" ");
        rovers.add(Rover
            .builder()
            .abscissa(Integer.parseInt(position[0]))
            .ordinate(Integer.parseInt(position[1]))
            .direction(
                CardinalCompass.valueOf(position[2]))
            .instructions(allLines.get(i + 1))
            .build());
      }

    } catch (IllegalArgumentException | FileSystemNotFoundException | IOException e) {
      myLog.info(e.getMessage());
    }
    return rovers;
  }
}
