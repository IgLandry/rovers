package org.igor.services;

import java.util.List;
import org.igor.models.Rover;

public interface FileServices {

  List<Rover> readInputFile(String inputFile);
}
