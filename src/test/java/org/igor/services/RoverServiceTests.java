package org.igor.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.igor.models.Rover;
import org.igor.models.types.CardinalCompass;
import org.igor.services.implementations.RoverServicesImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
class RoverServiceTests {

  private RoverServices roverServices;

  @BeforeAll
  void setup() {

    roverServices = new RoverServicesImpl();
  }

  @Test
  void givenRoverWhenTurnToTheRightThenChangeDirection() {
    //GIVEN
    Rover rover = Rover
        .builder()
        .abscissa(1)
        .ordinate(2)
        .direction(CardinalCompass.W)
        .build();
    //WHEN
    roverServices.turnToTheRight(rover);
    //THEN
    assertEquals(CardinalCompass.N, rover.getDirection());
  }

  @Test
  void givenRoverWhenTurnToTheLeftThenChangeDirection() {
    //GIVEN
    Rover rover = Rover
        .builder()
        .abscissa(1)
        .ordinate(2)
        .direction(CardinalCompass.W)
        .build();
    //WHEN
    roverServices.turnToTheLeft(rover);
    //THEN
    assertEquals(CardinalCompass.S, rover.getDirection());
  }

  @Test
  void givenRoverWhenStepForwardThenIncrementOrDecrementXorY() {
    //GIVEN
    Rover rover = Rover
        .builder()
        .abscissa(1)
        .ordinate(2)
        .direction(CardinalCompass.E)
        .build();
    //WHEN
    roverServices.stepForward(rover, Rover
        .builder()
        .abscissa(5)
        .ordinate(5)
        .build());
    //THEN
    assertEquals(2, rover.getAbscissa());
  }
}
